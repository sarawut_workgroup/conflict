/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarawut.conflict;

/**
 *
 * @author Sarawut@ulknows
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnSelect = new javax.swing.JPanel();
        btnMax = new javax.swing.JButton();
        btnKing = new javax.swing.JButton();
        btnLife = new javax.swing.JButton();
        scrShow = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnMax.setText("Max");
        btnMax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMaxActionPerformed(evt);
            }
        });

        btnKing.setText("King");
        btnKing.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKingActionPerformed(evt);
            }
        });

        btnLife.setText("Life");
        btnLife.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLifeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpnSelectLayout = new javax.swing.GroupLayout(jpnSelect);
        jpnSelect.setLayout(jpnSelectLayout);
        jpnSelectLayout.setHorizontalGroup(
            jpnSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnSelectLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnMax)
                .addGap(18, 18, 18)
                .addComponent(btnKing)
                .addGap(18, 18, 18)
                .addComponent(btnLife)
                .addContainerGap(639, Short.MAX_VALUE))
        );
        jpnSelectLayout.setVerticalGroup(
            jpnSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnSelectLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMax)
                    .addComponent(btnKing)
                    .addComponent(btnLife))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jpnSelect, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(scrShow))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jpnSelect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(scrShow, javax.swing.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

            private void btnMaxActionPerformed(java.awt.event.ActionEvent evt) {                
                scrShow.setViewportView(new PanelMax());
            }            
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        
    }//GEN-LAST:event_jButton2ActionPerformed
            

    private void btnLifeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLifeActionPerformed
        scrShow.setViewportView(new LifePanel());
    }//GEN-LAST:event_btnLifeActionPerformed

    private void btnKingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKingActionPerformed
        
        scrShow.setViewportView(new King());
    }//GEN-LAST:event_btnKingActionPerformed

            /**
             * @param args the command line arguments
             */
            public static void main(String args[]) {
                /* Set the Nimbus look and feel */
                //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
                /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
                 */
                try {
                    for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                        if ("Nimbus".equals(info.getName())) {
                            javax.swing.UIManager.setLookAndFeel(info.getClassName());
                            break;
                        }
                    }
                } catch (ClassNotFoundException ex) {
                    java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                    java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                //</editor-fold>

                /* Create and display the form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        new MainFrame().setVisible(true);
                    }
                });
            }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnKing;
    private javax.swing.JButton btnLife;
    private javax.swing.JButton btnMax;
    private javax.swing.JPanel jpnSelect;
    private javax.swing.JScrollPane scrShow;
    // End of variables declaration//GEN-END:variables
}
